-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 14, 2022 at 03:09 AM
-- Server version: 8.0.29
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appsero`
--

-- --------------------------------------------------------

--
-- Table structure for table `integrations`
--

CREATE TABLE `integrations` (
  `id` int UNSIGNED NOT NULL,
  `provider` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_id` bigint UNSIGNED NOT NULL,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires` int UNSIGNED NOT NULL,
  `extra` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `owner_id` int UNSIGNED NOT NULL,
  `owner_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `integrations`
--

INSERT INTO `integrations` (`id`, `provider`, `profile_id`, `username`, `name`, `avatar`, `token`, `token_secret`, `expires`, `extra`, `owner_id`, `owner_type`, `created_at`, `updated_at`) VALUES
(5, 'fastspring', 0, 'MZLHYSO8QOGH-HMRVXLXRQ', 'MZLHYSO8QOGH-HMRVXLXRQ', '', '', 'fTAnvQdPR3O5hX-hN4N5gw', 0, NULL, 2, 'App\\User', '2022-04-21 23:22:39', '2022-04-21 23:22:39'),
(6, 'paddle', 0, '5875', '5875', '', '258532276b148b6b3b51fc6d7dc0723d7665a7e90ade95a71d', '', 0, '{\"sandbox\":true}', 2, 'App\\User', '2022-04-25 03:30:23', '2022-04-25 03:30:23'),
(14, 'mailchimp', 141239474, 'anis3139', 'Web Developer', '', '04a1bd936d647784b3b6ff50d25c0bbc-us4', '', 0, NULL, 4, 'App\\User', '2022-05-17 23:46:10', '2022-05-17 23:46:10'),
(15, 'mailchimp-ecommerce', 0, 'anis3139', 'Anis', '', '04a1bd936d647784b3b6ff50d25c0bbc-us4', '6256748590d99', 0, NULL, 4, 'App\\User', '2022-05-24 03:34:47', '2022-06-09 03:56:08'),
(16, 'github', 38912435, 'anis3139', 'MD. Anichur Rahaman', 'https://avatars.githubusercontent.com/u/38912435?v=4', 'ghu_Kp07niQPvlMmqOE5VFMcNYf41WvMnW4FvdnZ', '', 0, NULL, 5, 'App\\User', '2022-05-25 03:36:37', '2022-05-25 03:36:37'),
(17, 'github', 38912435, 'anis3139', 'MD. Anichur Rahaman', 'https://avatars.githubusercontent.com/u/38912435?v=4', 'ghu_G3LrzUgXaENtz0DIIhCDVtA7YcR07f4GSoHV', '', 0, NULL, 4, 'App\\User', '2022-05-26 22:50:29', '2022-05-26 22:50:29'),
(19, 'envato', 0, 'anis3139', 'Md. Anichur  Rahaman', '', 'T12GeC4cFIG3KNDoSe2vF2DnNJbAB0Y0', '', 0, NULL, 4, 'App\\User', '2022-05-26 22:51:52', '2022-05-26 22:51:52'),
(22, 'fastspring', 0, 'KUGZWRWMTDE8ZIGRNFANWW', 'KUGZWRWMTDE8ZIGRNFANWW', '', '', 'e-CdSfbQQny3yciRJXinzA', 0, NULL, 4, 'App\\User', '2022-06-02 03:32:55', '2022-06-02 03:32:55'),
(23, 'wporg', 0, 'dmsanis3139', 'dmsanis3139', 'https://secure.gravatar.com/avatar/aa960d105a0cae8b16a2873f6f348ced?s=100&#038;d=mm&#038;r=g', '', '', 0, '{\"username\":\"dmsanis3139\",\"password\":\"eyJpdiI6Ikg1TVNLa0ZFekpOZXRaZitZNDBDM1E9PSIsInZhbHVlIjoibHZTQytoNytTUVAxV3hpY1R5RnB4dz09IiwibWFjIjoiODIzZGEyODI2ZTFhNDgyMDU5YTIxMzAxNDY1NzQzODQ4YWZkMGNkNGY0MTUyMGM4NTFiNmUzNzUyMjVhYzViZCJ9\"}', 4, 'App\\User', '2022-06-06 03:26:19', '2022-06-06 03:26:19'),
(24, 'gumroad', 0, 'anis3139', 'আনিছ আরণ্য', '', 'HJONWObUVKJ_STdIqidfzOU4blzbiSS1dZ4VoITIBjk', '', 0, '{\"name\":\"\\u0986\\u09a8\\u09bf\\u099b \\u0986\\u09b0\\u09a3\\u09cd\\u09af\",\"currency_type\":\"usd\",\"custom_css\":null,\"bio\":\"\",\"twitter_handle\":null,\"id\":\"3045685939410\",\"user_id\":\"I3EeNjDiGTytqRx4bcHbAw==\",\"url\":\"https:\\/\\/app.gumroad.com\\/anis3139\",\"links\":[],\"profile_url\":\"https:\\/\\/assets.gumroad.com\\/assets\\/gumroad-default-avatar-5-85b6f5cbafced321e1a851db194f33f91f9ed5ac0bd1ca578a8f298413ce3d9b.png\",\"email\":\"anis904692@gmail.com\",\"display_name\":\"\\u0986\\u09a8\\u09bf\\u099b \\u0986\\u09b0\\u09a3\\u09cd\\u09af\"}', 4, 'App\\User', '2022-06-08 23:26:09', '2022-06-08 23:26:09'),
(26, 'helpscout', 0, '', '', '', 'd1eaaee4-e7de-11ec-b9e3-9f3fb98a669f', 'bcfa7fed-c193-401d-ae89-207ab84e1166', 0, NULL, 4, 'App\\User', '2022-06-09 04:27:53', '2022-06-09 04:27:53'),
(27, 'paddle', 0, '5875', '5875', '', '258532276b148b6b3b51fc6d7dc0723d7665a7e90ade95a71d', '', 0, '{\"sandbox\":true}', 4, 'App\\User', '2022-06-09 04:49:44', '2022-06-09 04:49:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `integrations`
--
ALTER TABLE `integrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `integrations_provider_index` (`provider`),
  ADD KEY `integrations_owner_id_index` (`owner_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `integrations`
--
ALTER TABLE `integrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
